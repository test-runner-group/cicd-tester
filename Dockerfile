FROM php:${PHP_VERSION:-8.2}-fpm

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions && \
IPE_ICU_EN_ONLY=1 install-php-extensions intl json zip pdo_pgsql gd xdebug \
yaml redis apcu calendar zip intl imagick amqp rdkafka bz2

#install composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php -r "if (hash_file('sha384', 'composer-setup.php') === 'e21205b207c3ff031906575712edab6f13eb0b361f2085f1f1237b7126d785e826a450292b6cfd1d64d92e6563bbde02') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php && \
    php -r "unlink('composer-setup.php');" && mv composer.phar /usr/local/bin/composer

RUN printf "# composer php cli ini settings\n\
date.timezone=UTC\n\
memory_limit=512M\n\
" > $PHP_INI_DIR/conf.d/php-custom.ini

RUN apt-get update && apt-get install -y wget && apt-get clean && rm -rf /var/lib/apt/lists/* \
&& wget https://get.symfony.com/cli/installer -O - | bash && ln -s /root/.symfony5/bin/symfony /usr/local/bin/symfony
#RUN , php-pear pear install php_codesniffer && pear install phpmd/PHP_PMD

RUN mkdir app
COPY . ./app
WORKDIR ./app

RUN composer install && symfony server:ca:install

EXPOSE 8000

CMD ["symfony", "serve"]
#ENTRYPOINT ["docker-php-entrypoint"]

