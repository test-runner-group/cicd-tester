<?php

namespace App\Tests\Unit\Service;

use PHPUnit\Framework\TestCase;

class UtilsTestPhpTest extends TestCase
{
    public function testProjectHasAnDockerfile(): void
    {
        $this->assertFileExists(sprintf('%s/../../../%s', __DIR__, 'Dockerfile'));
    }

    public function testProjectHasTemplateDir(): void
    {
        $this->assertDirectoryExists(sprintf('%s/../../../%s', __DIR__, 'templates'));
    }

    public function testAssertStringIsDifferentOfNumber(): void
    {
        $this->assertEquals('42', 42);
    }
}
